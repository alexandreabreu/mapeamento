class AddNomeCpfToPessoas < ActiveRecord::Migration
  def up
    add_column :pessoas, :nome, :string
    add_column :pessoas, :cpf, :string
  end
  
  def down
    remove_column :pessoas, :nome
    remove_column :pessoas, :cpf
  end

end
