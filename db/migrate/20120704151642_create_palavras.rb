class CreatePalavras < ActiveRecord::Migration
  def change
    create_table :palavras do |t|
      t.string :expressao
      t.integer :rede_id

      t.timestamps
    end
  end
end
