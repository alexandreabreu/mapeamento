class CreateUnidades < ActiveRecord::Migration
  def change
    create_table :unidades do |t|
      t.string :nome
      t.string :logradouro
      t.integer :numero
      t.string :bairro
      t.string :cidade
      t.string :uf
      t.string :complemento

      t.timestamps
    end
  end
end
