class CreateRedes < ActiveRecord::Migration
  def change
    create_table :redes do |t|
      t.string :nome

      t.timestamps
    end
  end
end
