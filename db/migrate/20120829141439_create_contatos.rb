class CreateContatos < ActiveRecord::Migration
  def change
    create_table :contatos do |t|
      t.string :telefone
      t.string :celular
      t.string :email
      t.belongs_to :obj, polymorphic: true
      #t.string :obj_type
      #t.integer :obj_id

      t.timestamps
    end
    add_index :contatos, [:obj_id, :obj_type]
  end
end
