class CreateDepartamentos < ActiveRecord::Migration
  def change
    create_table :departamentos do |t|
      t.string :nome
      t.integer :unidade_id

      t.timestamps
    end
  end
end
