class CreateLaboratorios < ActiveRecord::Migration
  def change
    create_table :laboratorios do |t|
      t.string :nome
      t.string :sigla
      t.text :descricao

      t.timestamps
    end
  end
end
