class AddDepartamentoIdToLaboratorios < ActiveRecord::Migration
  def change
    add_column :laboratorios, :departamento_id, :integer
  end
end
