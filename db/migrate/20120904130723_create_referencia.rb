class CreateReferencia < ActiveRecord::Migration
  def change
    create_table :referencia do |t|
      t.integer :palavra_id
      t.belongs_to :obj, polymorphic: true
      #t.integer :obj_id
      #t.string :obj_type

      t.timestamps
    end
    add_index :referencia, [:obj_id, :obj_type]
  end
end
