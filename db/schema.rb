# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120924133329) do

  create_table "contatos", :force => true do |t|
    t.string   "telefone"
    t.string   "celular"
    t.string   "email"
    t.integer  "obj_id"
    t.string   "obj_type"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "contatos", ["obj_id", "obj_type"], :name => "index_contatos_on_obj_id_and_obj_type"

  create_table "departamentos", :force => true do |t|
    t.string   "nome"
    t.integer  "unidade_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "laboratorios", :force => true do |t|
    t.string   "nome"
    t.string   "sigla"
    t.text     "descricao"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.integer  "departamento_id"
  end

  create_table "palavras", :force => true do |t|
    t.string   "expressao"
    t.integer  "rede_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "pessoas", :force => true do |t|
    t.string   "username",                        :null => false
    t.string   "email"
    t.string   "crypted_password"
    t.string   "salt"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.string   "remember_me_token"
    t.datetime "remember_me_token_expires_at"
    t.string   "reset_password_token"
    t.datetime "reset_password_token_expires_at"
    t.datetime "reset_password_email_sent_at"
    t.string   "nome"
    t.string   "cpf"
  end

  add_index "pessoas", ["remember_me_token"], :name => "index_pessoas_on_remember_me_token"
  add_index "pessoas", ["reset_password_token"], :name => "index_pessoas_on_reset_password_token"

  create_table "redes", :force => true do |t|
    t.string   "nome"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.text     "descricao"
  end

  create_table "referencia", :force => true do |t|
    t.integer  "palavra_id"
    t.integer  "obj_id"
    t.string   "obj_type"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "referencia", ["obj_id", "obj_type"], :name => "index_referencia_on_obj_id_and_obj_type"

  create_table "unidades", :force => true do |t|
    t.string   "nome"
    t.string   "logradouro"
    t.integer  "numero"
    t.string   "bairro"
    t.string   "cidade"
    t.string   "uf"
    t.string   "complemento"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

end
