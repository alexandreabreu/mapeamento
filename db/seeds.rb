#!/bin/env ruby
# encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Unidade.destroy_all
Departamento.destroy_all

Unidade.create [
  { nome: "Computação", numero: 122, logradouro: "Campus da Praia Vermelha", bairro: "Ingá", cidade: "Niterói", uf: "RJ"},
  { nome: "Física", numero: 122, logradouro: "Campus da Praia Vermelha", bairro: "Ingá", cidade: "Niterói", uf: "RJ"},
  { nome: "Geociências", numero: 122, logradouro: "Campus da Praia Vermelha", bairro: "Ingá", cidade: "Niterói", uf: "RJ"},
  { nome: "Química", numero: 34, logradouro: "Campus do Valonguinho", bairro: "Ingá", cidade: "Niterói", uf: "RJ"},
  { nome: "Biologia", numero: 34, logradouro: "Campus do Valonguinho", bairro: "Centro", cidade: "Niterói", uf: "RJ"},
  { nome: "Matemática", numero: 34, logradouro: "Campus do Valonguinho", bairro: "Centro", cidade: "Niterói", uf: "RJ"}
  ]

computacao = Unidade.where(nome: "Computação").first
Departamento.create [
  {nome: "Ciência da Computação", unidade: computacao},
  {nome: "Sistemas de Informação", unidade: computacao}
]
