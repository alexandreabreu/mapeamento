require 'test_helper'

class PalavraControllerTest < ActionController::TestCase
  test "should get expressao" do
    get :expressao
    assert_response :success
  end

  test "should get rede_id:integer" do
    get :rede_id:integer
    assert_response :success
  end

end
