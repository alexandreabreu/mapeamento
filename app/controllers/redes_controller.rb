class RedesController < ApplicationController
  # GET /redes
  # GET /redes.json
  def index
    @redes = Rede.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @redes }
    end
  end

  # GET /redes/new
  # GET /redes/new.json
  def new
    @rede = Rede.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @rede }
    end
  end

  # GET /redes/1/edit
  def edit
    @rede = Rede.find(params[:id])
  end

  # POST /redes
  # POST /redes.json
  def create
    @rede = Rede.new(params[:rede])

    respond_to do |format|
      if @rede.save
        #msg sucesso
        format.html { redirect_to redes_path }
      else
        #msg erro
        format.html { render action: "new" }
      end
    end
  end

  # PUT /redes/1
  # PUT /redes/1.json
  def update
    @rede = Rede.find(params[:id])

    respond_to do |format|
      if @rede.update_attributes(params[:rede])
        format.html { redirect_to redes_path }
        #msg sucessi
      else
        format.html { render action: "edit" }
        #msg erro
      end
    end
  end

  # DELETE /redes/1
  # DELETE /redes/1.json
  def destroy
    @rede = Rede.find(params[:id])
    @rede.destroy

    respond_to do |format|
      format.html { redirect_to redes_url }
      format.json { head :no_content }
    end
  end
end
