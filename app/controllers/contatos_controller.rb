class ContatosController < ApplicationController
  before_filter :carregar_obj, only: [:new, :edit]
  
  def new
    @contato = Contato.new
    @contato.obj = @obj
  end

  def edit
    @contato = Contato.find(params[:id])
  end

  def create
    @contato = Contato.new(params[:contato])
    @contato.save ? redirect_to(@contato.obj) : render(action: "new") 
  end

  def update
    @contato = Contato.find(params[:id])
    @contato.update_attributes(params[:contato]) ? redirect_to(@contato.obj) : render(action: "edit")
  end

  def destroy
    @contato = Contato.find(params[:id])
    @contato.destroy
    redirect_to @obj
  end

  private

  def carregar_obj
    klass = eval(params[:obj_type])
    @obj = klass.find params[:obj_id]
  end

end
