class PalavrasController < ApplicationController
  before_filter :get_rede
  
  def get_rede
    @rede = Rede.find params[:rede_id] if params[:rede_id]
  end
  
  # GET /palavras
  # GET /palavras.json
  def index
    if @rede
      @palavras = @rede.palavras
      @palavra = Palavra.new
    else
      @palavras = Palavra.where("expressao like ?", "%#{params[:q]}%")
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json  do
        #render json: @palavras
        render json: @palavras.to_json(only: [:id, :expressao])
       end
    end
  end

  # GET /palavras/1
  # GET /palavras/1.json
  def show
    @palavra = Palavra.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @palavra }
    end
  end

  # GET /palavras/new
  # GET /palavras/new.json
  def new
    @palavra = Palavra.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @palavra }
    end
  end

  # GET /palavras/1/edit
  def edit
    @palavra = Palavra.find(params[:id])
  end

  # POST /palavras
  # POST /palavras.json
  def create
    @palavra = Palavra.new(params[:palavra])
    @palavra.rede = @rede

    respond_to do |format|
      if @palavra.save
        format.html { redirect_to rede_palavras_path(@rede) }
        format.json { render json: @palavra, status: :created, location: @palavra }
      else
        format.html { redirect_to rede_palavras_path(@rede) }
        format.json { render json: @palavra.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /palavras/1
  # PUT /palavras/1.json
  def update
    @palavra = Palavra.find(params[:id])

    respond_to do |format|
      if @palavra.update_attributes(params[:palavra])
        format.html { redirect_to @palavra, notice: 'Palavra was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @palavra.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /palavras/1
  # DELETE /palavras/1.json
  def destroy
    @palavra = Palavra.find(params[:id])
    @palavra.destroy

    respond_to do |format|
      format.html { redirect_to rede_palavras_path(@rede) }
      format.json { head :no_content }
    end
  end
end

  