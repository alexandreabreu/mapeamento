#!/bin/env ruby
# encoding: utf-8

module ApplicationHelper
  
  def carregar_menu(path)
    begin
      classe = /(\w+)(?=s)/.match(path)[1].capitalize.constantize
    rescue
      return nil
    end
    return classe.to_s
  end
  
  def botao_com_icone(texto = "", link = "#", icone = "icon-plus", args={})
    classe_icone = icone
    html_icone = content_tag(:i, "", class: classe_icone)
    html_btn = "btn #{args[:btn_class] if args[:btn_class]}"
    botao = link_to (html_icone + " " + texto), link, class: html_btn
  end
  
  def bloco_vazio (texto = "", btn = "", args={})
      titulo = content_tag :h3, "Vazio!" 
      p = content_tag :p, texto
      content_tag :div, (titulo + tag(:hr) + p + btn ), args
  end
  
  def flash_notice
    html = ""
    unless flash.empty?
      flash.each do |f|
        html << "<div class='alert alert-#{f[0].to_s}'>"
        html << "<a class='close' data-dismiss='alert'>×</a>"
        html << f[1].to_s
        html << "</div>"
      end
    else
      nil
    end
  end

  def bloco_contato(args={})
    contato = args[:contato]
    obj = args[:obj]
    html_class = args[:class] ? args[:class] : "" 
    quebra = tag :br
    if obj.contato.nil?
      botao = botao_com_icone "Cadastrar Contatos", novo_contato_path(obj.class, obj.id), "icon-plus icon-white", btn_class: "btn btn-primary btn-large" 
      bloco_vazio "Contatos não cadastrados", botao, class: "well span5 construcao"
    else
      
      botao = botao_com_icone "Editar", editar_contato_path(contato, obj.class, obj.id), "icon-edit icon-white", btn_class: "btn btn-mini btn-primary edit-button"
      btn_wraper = content_tag :small, botao
      texto_titulo = "Contatos"
      titulo = content_tag :h3, (texto_titulo + btn_wraper).html_safe
      telefone = content_tag(:i, "",class: "icon-th") + " Telefone: " + contato.telefone
      celular = content_tag(:i, "",class: "icon-th") + " Telefone: " + contato.celular
      email_icone = content_tag(:i, "",class: "icon-envelope")
      email_link = link_to @contato.email, "mailto:#{@contato.email}"
      email = email_icone + " Email: " + email_link
      bloco = content_tag :address, (telefone + quebra + celular + quebra + email)
      html_class += " well"
      s = content_tag :div, (titulo + tag(:hr) + bloco), id: "contato", class: html_class
    end
  end

end
