class Referencia < ActiveRecord::Base
  attr_accessible :obj_id, :obj_type, :palavra_id, :palavra
  belongs_to :obj, polymorphic: true
  belongs_to :palavra
  
  validates :obj, :palavra_id, presence: true
end
