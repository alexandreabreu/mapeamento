class Palavra < ActiveRecord::Base
  attr_accessible :expressao, :rede_id
  belongs_to :rede
  has_many :referencias, dependent: :destroy
  has_many :laboratorios, through: :referencias
  
  validates :expressao, presence: true
end
