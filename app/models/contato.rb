class Contato < ActiveRecord::Base
  attr_accessible :celular, :email, :telefone, :obj_id, :obj_type
  belongs_to :obj, polymorphic: true
end
