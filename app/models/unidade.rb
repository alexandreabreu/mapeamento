class Unidade < ActiveRecord::Base
  attr_accessible :bairro, :cidade, :complemento, :logradouro, :nome, :numero, :uf
  validates :nome, presence: true
  has_many :departamentos
  has_one :contato, as: :obj

  def to_s
    nome
  end
end
