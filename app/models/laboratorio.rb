class Laboratorio < ActiveRecord::Base
  attr_accessible :descricao, :nome, :sigla, :departamento_id, :palavra_tokens
  belongs_to :departamento
  has_one :contato, as: :obj
  has_many :referencias, as: :obj
  has_many :palavras, through: :referencias
  has_many :redes, through: :palavras
  attr_reader :palavra_tokens
    
  validates :departamento, presence: true

  def to_s
    sigla? ? "#{nome} - #{sigla}" : "#{nome}"
  end
  
  def palavra_tokens=(ids)
    self.palavra_ids = ids.split(",")
  end

end
