class Rede < ActiveRecord::Base
  attr_accessible :nome
  attr_accessible :descricao
  has_many :palavras
end
