class Departamento < ActiveRecord::Base
  attr_accessible :nome, :unidade_id, :unidade
  belongs_to :unidade
  has_many :laboratorios
  has_one :contato, as: :obj
  
  def to_s
    "#{nome}"
  end
end
