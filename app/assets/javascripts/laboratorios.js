	var redes = {};
	var coisaChaveDelay = 100
$(document).ready(function(){
	$(".rede-chave").each( function(){
		redes[$(this).attr('id')] = $(this)
	});
});


$(function () {
  $('#laboratorio_palavra_tokens').tokenInput('/palavras.json', { 
    crossDomain: false,
    prePopulate: $('#laboratorio_palavra_tokens').data('pre'),
    theme: 'facebook'
  });
});

$(function () {
	$(".palavra-chave").hover(
		function (){
			$(this).addClass('palavra-selecionada', coisaChaveDelay);
			var rede = $(this).attr("data-rede");
			$(redes[rede]).addClass('rede-selecionada', coisaChaveDelay);
		},
		function (){
			$(this).removeClass('palavra-selecionada', coisaChaveDelay);
			var rede = $(this).attr("data-rede");
			$(redes[rede]).removeClass('rede-selecionada', coisaChaveDelay);
		}
	);
});

$(function () {
	$(".rede-chave").hover(
		function (){
			$(this).addClass('rede-selecionada', coisaChaveDelay);
			var rede = $(this).attr("id");
			$(".palavra-chave").each( function(){
				if ($(this).attr("data-rede") === rede){
					$(this).addClass('palavra-selecionada', coisaChaveDelay);
				};
			});
		},
		function (){
			$(this).removeClass('rede-selecionada', coisaChaveDelay);
			var rede = $(this).attr("id");
			$(".palavra-chave").each( function(){
				if ($(this).attr("data-rede") === rede){
					$(this).removeClass('palavra-selecionada', coisaChaveDelay);
				};
			});
		}
	);
});	